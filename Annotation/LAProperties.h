//
//  LAProperties.h
//  Writeability
//
//  Created by Ryan on 10/4/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>


// TODO: Get rid of LProperties class

@interface LAProperties : NSObject

@property (nonatomic, readwrite, assign) Class      type;

@property (nonatomic, readwrite, strong) UIColor    *color;
@property (nonatomic, readwrite, assign) float      alpha;

@property (nonatomic, readwrite, strong) UIFont     *font;

@property (nonatomic, readwrite, assign) float      weight;
@property (nonatomic, readwrite, assign) float      multiplier;


+ (Class)classWithInfo:(NSDictionary *)info;


- (UIColor *)colorWithAlpha;
- (UIColor *)colorWithPremultipliedAlpha;

- (float)scaledWeight;

- (NSDictionary *)info;
- (void)setInfo:(NSDictionary *)info;

@end
