//
//  LABezier.h
//  Writeability
//
//  Created by Ryan on 1/3/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Rendering/LGLUtilities.h>

#import <vector>


extern void LABezierQuadric
(
 LGLVector2                 start,
 LGLVector2                 control,
 LGLVector2                 end,
 std::vector<LGLVector2>    &points
 );
