//
//  LAWeightedLine.h
//  Writeability
//
//  Created by Ryan on 1/3/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Rendering/LGLUtilities.h>

#import <vector>


extern void LAWeightedLineSegment
(
 LGLVector2         head,
 LGLVector2         tail,
 float              headWeight,
 float              tailWeight,
 std::vector<float> *body,
 std::vector<float> *headCap,
 std::vector<float> *tailCap
 );
