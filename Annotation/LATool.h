//
//  LATool.h
//  Writeability
//
//  Created by Ryan on 8/27/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LAProtocol.h"

#import "LAProperties.h"



@interface LATool : NSObject <LAProtocol>

@property (nonatomic, readonly, strong) NSData			*buffer;

@property (nonatomic, readonly, assign) CGPoint         currentLocation;

@property (nonatomic, readonly, strong) LAProperties	*properties;


+ (BOOL)isTypePen:(Class)type;
+ (BOOL)isTypeHiliter:(Class)type;
+ (BOOL)isTypeEraser:(Class)type;
+ (BOOL)isTypeSelector:(Class)type;

+ (LATool *)newForType:(Class)type;

+ (LATool *)newPen;
+ (LATool *)newHighlighter;
+ (LATool *)newEraser;
+ (LATool *)newBoxer;


- (BOOL)isMarker;
- (BOOL)isShaper;
- (BOOL)isEditor;

- (BOOL)isPen;
- (BOOL)isHiliter;
- (BOOL)isEraser;
- (BOOL)isBoxer;

@end
