//
//  LAProperties.m
//  Writeability
//
//  Created by Ryan on 10/4/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LAProperties.h"

#import <Utilities/LMacros.h>

#import <Utilities/UIColor+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LAProperties
#pragma mark -
//*********************************************************************************************************************//




@implementation LAProperties

#pragma mark - Static Objects
//*********************************************************************************************************************//

static NSString *const kTPType          = @"type";
static NSString *const kTPColor         = @"color";
static NSString *const kTPFont          = @"font";
static NSString *const kTPWeight        = @"weight";
static NSString *const kTPMultiplier    = @"multiplier";


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (NSDictionary *)typeMapping
{
    static dispatch_once_t  onceToken       = 0;
    static NSDictionary     *typeMapping    = nil;

    dispatch_once(&onceToken, ^{
        typeMapping = @{
            @"NSNull"       : @"N",
            @"LAPenTool"     : @"P",
            @"LAHiliterTool" : @"H",
            @"LAEraserTool"  : @"E",
            @"LTextTool"    : @"T"
        };
    });

    return typeMapping;
}

+ (Class)classWithInfo:(NSDictionary *)info
{
    NSString        *type           = info[ kTPType ];
    NSDictionary    *typeMapping    = [LAProperties typeMapping];

    for (NSString *key in typeMapping) {
        NSString *value = typeMapping[ key ];

        if ([type isEqualToString:value]) {
            return NSClassFromString(key);
        }
    }

    return nil;
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init
{
    if ((self = [super init])) {
        _type       = [NSNull class];
        _color      = [UIColor blackColor];
        _alpha      = 0.;
        _multiplier = 1.;
    }

    return self;
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setFont:(UIFont *)font
{
    if (![_font isEqual:font]) {
        {_font = font;}

        self.weight = [font pointSize];
    }
}

- (void)setWeight:(float)weight
{
    if (!IsEqual(_weight, weight)) {
        {_weight = weight;}

        if ([self font]) {
            self.font = [self.font fontWithSize:weight];
        }
    }
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (UIColor *)colorWithAlpha
{
    return [_color colorWithAlphaComponent:_alpha];
}

- (UIColor *)colorWithPremultipliedAlpha
{
    CGFloat components[4];
    [_color getComponents:components];

    components[3] = _alpha;

    return [UIColor
            colorWithRed    :(components[0] * components[3])
            green           :(components[1] * components[3])
            blue            :(components[2] * components[3])
            alpha           :(components[3])];
}

- (float)scaledWeight
{
    return (_weight * _multiplier);
}


#pragma mark - Network
//*********************************************************************************************************************//

- (NSDictionary *)info
{
	CGFloat components[4];
    [_color getComponents:components];

    components[3] = _alpha;

    NSInteger red       = (255* components[0]);
    NSInteger green     = (255* components[1]);
    NSInteger blue      = (255* components[2]);
    NSInteger alpha     = (255* components[3]);

    NSString *encoded   = @Format(@"%02lX%02lX%02lX%02lX", red, green, blue, alpha);
    NSString *mapped    = [LAProperties typeMapping][ NSStringFromClass(_type) ];
    
    return [self font]?
    (@{ kTPColor        : encoded,
        kTPFont         : [_font fontName],
        kTPWeight       : @(_weight),
        kTPMultiplier   : @(_multiplier),
        kTPType         : mapped }):
    (@{ kTPColor        : encoded,
        kTPWeight       : @(_weight),
        kTPMultiplier   : @(_multiplier),
        kTPType         : mapped });
}

- (void)setInfo:(NSDictionary *)info
{
    NSString *color = info[ kTPColor ];
    NSString *type  = info[ kTPType ];

    _weight     = [info[ kTPWeight ] floatValue];
    _multiplier = [info[ kTPMultiplier ] floatValue];

    _font       = [UIFont fontWithName:info[ kTPFont ] size:[self weight]];

    NSScanner   *scanner    = [NSScanner scannerWithString:color];
    NSUInteger  colorCode   = 0;
    
    [scanner scanHexInt:&colorCode];

    NSInteger red       = ((colorCode >> 24) & 0xFF);
    NSInteger green     = ((colorCode >> 16) & 0xFF);
    NSInteger blue      = ((colorCode >>  8) & 0xFF);
    NSInteger alpha     = ((colorCode & 0xFF));
	
	_alpha = (alpha /255.);

	_color = [UIColor
              colorWithRed  :(red   /255.)
              green         :(green /255.)
              blue          :(blue  /255.)
              alpha         :1.];

    [[LAProperties typeMapping]
     enumerateKeysAndObjectsWithOptions :NSEnumerationConcurrent
     usingBlock                         :
     ^(NSString *key, NSString *value, BOOL *stop) {
         if ([type isEqualToString:value]) {
             _type = NSClassFromString(key);

             *stop = YES;
         }
    }];
}


@end
