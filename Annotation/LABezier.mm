//
//  File.cpp
//  Writeability
//
//  Created by Ryan on 1/3/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LABezier.h"

#import <Utilities/LFastMath.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LABezier
#pragma mark -
// Algorithm derived from the Anti-Grain Geometry Project @ http://antigrain.com/research/adaptive_bezier
//*********************************************************************************************************************//




#pragma mark - Static Objects
//*********************************************************************************************************************//

static const float          kLABezierCollinearityEpsilon			= .001;
static const float          kLABezierDistanceToleranceSquare		= .25;
static const float          kLABezierAngleTolerance				= 0.;
static const float          kLABezierCurveAngleToleranceEpsilon	= .01;
static const unsigned short	kLABezierRecursionLimit				= 32;


#pragma mark - Function Declarations
//*********************************************************************************************************************//

static void __LABezierQuadric
(
 LGLVector2                 start,
 LGLVector2                 control,
 LGLVector2                 end,
 std::vector<LGLVector2>    &points,
 unsigned int               level
 );


#pragma mark - Public Functions
//*********************************************************************************************************************//

void LABezierQuadric
(
 LGLVector2                 start,
 LGLVector2                 control,
 LGLVector2                 end,
 std::vector<LGLVector2>    &points
 ) {
    __LABezierQuadric(start, control, end, points, 0);
}


#pragma mark - Protected Functions
//*********************************************************************************************************************//

static void __LABezierQuadric
(
 LGLVector2                 start,
 LGLVector2                 control,
 LGLVector2                 end,
 std::vector<LGLVector2>    &points,
 unsigned int               level
 ) {
	if (level > kLABezierRecursionLimit) {
		return;
	}

	LGLVector2 startControl		= LGLVector2Lerp(start, control, .5);
	LGLVector2 controlEnd		= LGLVector2Lerp(control, end, .5);
	LGLVector2 startControlEnd	= LGLVector2Lerp(startControl, controlEnd, .5);

	LGLVector2 delta			= LGLVector2Subtract(end, start);

	float distance = LFMFabsf(((control.x - end.x) * delta.y -
                               (control.y - end.y) * delta.x));
	float angle;

	if (isgreater(distance, kLABezierCollinearityEpsilon)) {
		if (islessequal(distance * distance, kLABezierDistanceToleranceSquare * (delta.x * delta.x + delta.y * delta.y))) {
			if (isless(kLABezierAngleTolerance, kLABezierCurveAngleToleranceEpsilon)) {
                points.push_back(startControlEnd);

				return;
			}

			angle = LFMFabsf(LFMAtan2f(end.y - control.y, end.x - control.x) -
                             LFMAtan2f(control.y - start.y, control.x - start.x));

			if (isgreaterequal(angle, M_PI)) {
				angle = (2.* M_PI - angle);
			}

			if (isless(angle, kLABezierAngleTolerance)) {
                points.push_back(startControlEnd);

				return;
			}
		}
	} else {
#define distance2(startVector, endVector) \
        (LFMPowf((endVector.x - startVector.x), 2.) + LFMPowf((endVector.y - startVector.y), 2.))

		angle = (delta.x * delta.x + delta.y * delta.y);

		if (IsZero(angle)) {
			distance = distance2(start, control);
		} else {
			distance = LFMDivf(((control.x - start.x) * delta.x + (control.y - start.y) * delta.y), angle);

			if (isgreater(distance, 0.) && isless(distance, 1.)) {
				return;
			}

			if (islessequal(distance, 0.)) {
				distance = distance2(control, start);
			} else if (isgreaterequal(distance, 1.)) {
				distance = distance2(control, end);
			} else {
				distance = distance2(control, ((LGLVector2) {
					.x = (start.x + distance * delta.x),
					.y = (start.y + distance * delta.y)
				}));
			}
		}
#undef distance2
		if (isless(distance, kLABezierDistanceToleranceSquare)) {
            points.push_back(control);

			return;
		}
	}

	__LABezierQuadric(start, startControl, startControlEnd, points, level +1);
	__LABezierQuadric(startControlEnd, controlEnd, end, points, level +1);
}
