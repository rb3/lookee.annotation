//
//  LAProtocol.h
//  Writeability
//
//  Created by Ryan on 8/27/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>


@protocol LAProtocol <NSObject>
@required

- (void)beginAtPoint:(CGPoint)point;
- (void)moveToPoint:(CGPoint)point;
- (void)endAtPoint:(CGPoint)point;

- (void)setPointData:(NSData *)data;
- (void)setVertexData:(NSData *)data;

@end


@protocol LAMarker	<LAProtocol> @end;
@protocol LAShaper	<LAProtocol> @end;
@protocol LAEditor	<LAProtocol> @end;