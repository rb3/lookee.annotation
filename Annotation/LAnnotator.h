//
//  LAnnotator.h
//  Writeability
//
//  Created by Ryan on 8/25/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LATool.h"



typedef BOOL(^LAnnotationType)(NSArray *);



/**
 * This class is only forward-declared, since its 
 * members do not need to be accessed directly.
 */
@class      LAnnotation;

@protocol	LADelegate;



@interface LAnnotator : NSObject

@property (nonatomic, readwrite	, weak  ) id                    <LADelegate>delegate;

@property (nonatomic, readwrite , strong) id                    tag;

@property (nonatomic, readonly  , strong) NSUndoManager         *undoManager;

@property (nonatomic, readonly	, assign) NSUInteger            annotationCount;
@property (nonatomic, readonly	, strong) LAnnotation           *lastAnnotation;

@property (nonatomic, readwrite	, strong) LATool                *annotationTool;

@property (nonatomic, readwrite	, assign, getter = isHidden)    BOOL hidden;


+ (instancetype)annotatorWithDelegate:(id <LADelegate>)delegate;

- (instancetype)initWithDelegate:(id <LADelegate>)delegate;


- (void)render;

- (NSUInteger)annotationIndexForPoint:(CGPoint)point;
- (LAnnotation *)annotationAtIndex:(NSUInteger)index;

- (void)createAnnotationWithTool:(LATool *)tool;

- (void)addAnnotation:(LAnnotation *)annotation;
- (void)insertAnnotation:(LAnnotation *)annotation atIndex:(NSUInteger)index;

- (void)addAnnotations:(NSArray *)annotations;
- (void)insertAnnotations:(NSArray *)annotations atIndexes:(NSIndexSet *)indexes;

- (void)removeAllAnnotations;
- (void)removeLastAnnotation;

- (void)removeAnnotation:(LAnnotation *)annotation;
- (void)removeAnnotationAtIndex:(NSUInteger)index;
- (void)removeAnnotationsAtIndexes:(NSIndexSet *)indexes;

@end


@protocol LADelegate <NSObject>
@required

- (void)annotatorDidUpdate:(LAnnotator *)annotator;

@end
