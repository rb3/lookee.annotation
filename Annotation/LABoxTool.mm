//
//  LShapeTool.m
//  Writeability
//
//  Created by Ryan on 12/3/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LABoxTool.h"

#import "LAWeightedLine.h"

#import <Utilities/LGeometry.h>

#import <vector>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LABoxTool
#pragma mark -
//*********************************************************************************************************************//




@interface LABoxTool ()

@property (nonatomic, readwrite , assign) LGLVector2 head;

@end


@implementation LABoxTool

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
    std::vector<GLfloat> _vertices;
}


#pragma mark - LAProtocol
//*********************************************************************************************************************//

- (void)initializeTool
{
}


- (GLvoid)beginAtVector:(LGLVector2)vector
{
    _vertices.clear();

    _head = vector;
}

- (GLvoid)moveToVector:(LGLVector2)vector
{
	_vertices.clear();

    CGRect  box     = CGRectFromPoints(_head.point, vector.point);

    CGPoint left    = CGRectGetLeftCenterPoint(box);
    CGPoint right   = CGRectGetRightCenterPoint(box);

    LGLVector2 start;
    LGLVector2 end;

    start.point = left;
    end.point   = right;

	LAWeightedLineSegment(start,
						 end,
						 box.size.height,
						 box.size.height,
						 &_vertices,
						 NULL,
						 NULL);

    [self setVertexData:WrapSTLVector(_vertices)];
}

- (GLvoid)endAtVector:(LGLVector2)vector
{
    [self moveToVector:vector];
}

@end
