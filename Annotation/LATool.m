//
//  LATool.m
//  Writeability
//
//  Created by Ryan on 8/27/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LATool.h"

#import "LAPenTool.h"
#import "LAHiliterTool.h"
#import "LAEraserTool.h"
#import "LABoxTool.h"

#import <Rendering/LGLUtilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LATool
#pragma mark -
//*********************************************************************************************************************//




@interface LATool ()

@property (nonatomic, readwrite, assign) CGPoint currentLocation;

@end

@implementation LATool

#pragma mark - Virtual Methods
//*********************************************************************************************************************//

- (void)initializeTool __VIRTUALMETHOD__;
- (GLvoid)beginAtVector:(LGLVector2)vector __VIRTUALMETHOD__;
- (GLvoid)moveToVector:(LGLVector2)vector __VIRTUALMETHOD__;
- (GLvoid)endAtVector:(LGLVector2)vector __VIRTUALMETHOD__;


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (BOOL)isTypePen:(Class)class
{
    return [class isSubclassOfClass:[LAPenTool class]];
}

+ (BOOL)isTypeHiliter:(Class)class
{
    return [class isSubclassOfClass:[LAHiliterTool class]];
}

+ (BOOL)isTypeEraser:(Class)class
{
    return [class isSubclassOfClass:[LAEraserTool class]];
}

+ (BOOL)isTypeSelector:(Class)class
{
    return [class isSubclassOfClass:[LABoxTool class]];
}

+ (LATool *)newForType:(Class)class
{
    return [[class alloc] __init];
}

+ (LATool *)newPen
{
	return [LAPenTool.alloc __init];
}

+ (LATool *)newHighlighter
{
	return [LAHiliterTool.alloc __init];
}

+ (LATool *)newEraser
{
	return [LAEraserTool.alloc __init];
}

+ (LATool *)newBoxer
{
    return [LABoxTool.alloc __init];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init __ILLEGALMETHOD__;

- (instancetype)__init
{
	if ((self = [super init])) {
		_properties = [LAProperties new];
        _properties.type = self.class;

        [self initializeTool];
	}
	
	return self;
}


#pragma mark - Properties
//*********************************************************************************************************************//

@synthesize buffer = _buffer;

- (void)setBuffer:(NSData *)buffer
{
    {_buffer = buffer;}
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (BOOL)isMarker
{
    return [self conformsToProtocol:@protocol(LAMarker)];
}

- (BOOL)isShaper
{
    return [self conformsToProtocol:@protocol(LAShaper)];
}

- (BOOL)isEditor
{
    return [self conformsToProtocol:@protocol(LAEditor)];
}

- (BOOL)isPen
{
    return [self isKindOfClass:[LAPenTool class]];
}

- (BOOL)isHiliter
{
    return [self isKindOfClass:[LAHiliterTool class]];
}

- (BOOL)isEraser
{
    return [self isKindOfClass:[LAEraserTool class]];
}

- (BOOL)isBoxer
{
    return [self isKindOfClass:[LABoxTool class]];
}


#pragma mark - LAProtocol
//*********************************************************************************************************************//

- (void)beginAtPoint:(CGPoint)point
{
    [self setCurrentLocation:point];

    self.buffer = nil;
    
    LGLVector2 vector = (LGLVector2)(point);
    LFMRounda(vector.v, vector.v, 2);
 
    [self beginAtVector:vector];
}

- (void)moveToPoint:(CGPoint)point
{
    [self setCurrentLocation:point];

    LGLVector2 vector = (LGLVector2)(point);
    LFMRounda(vector.v, vector.v, 2);

    [self moveToVector:vector];
}

- (void)endAtPoint:(CGPoint)point
{
    [self setCurrentLocation:point];

    LGLVector2 vector = (LGLVector2)(point);
    LFMRounda(vector.v, vector.v, 2);

    [self endAtVector:vector];
}

- (void)setPointData:(NSData *)data
{
    DBGParameterAssert(data.length > 0);

    const CGPoint   *point  = [data bytes];
    const CGPoint   *last   = [data bytes] + [data length] - (sizeof *point);

    [self beginAtPoint:*point];

    if (point != last) {
        while (++ point < last) {
            [self moveToPoint:*point];
        }
    }

    [self endAtPoint:*point];
}

- (void)setVertexData:(NSData *)data
{
    [self setBuffer:data];
}

@end
