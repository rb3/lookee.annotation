//
//  LAnnotator.m
//  Writeability
//
//  Created by Ryan on 8/25/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LAnnotator_Private.h"

#import <Rendering/LGL.h>
#import <Rendering/LGLShape.h>
#import <Rendering/LGLCanvas.h>

#import <Utilities/NSObject+Utilities.h>
#import <Utilities/UIColor+Utilities.h>
#import <Utilities/NSData+Utilities.h>

#import <OpenGLES/ES2/glext.h>

/** 
 * TODO: Make LAnnotations state-aware, so states are not set needlessly
 * TODO: Enclose operations on <annotations> and <discarded> in LGL operation block
 */




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//




@interface LAnnotation : LGLShape

@property (nonatomic, readwrite , strong) NSMutableArray    *actions;

@property (nonatomic, readonly  , assign, getter = isEmpty) BOOL empty;

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LAnnotator
#pragma mark -
//*********************************************************************************************************************//




@interface LAnnotator ()

@property (nonatomic, readwrite , assign, getter = isRendererDirty) BOOL rendererDirty;
@property (nonatomic, readwrite , assign, getter = isSelectionMode) BOOL selectionMode;
@property (nonatomic, readwrite , assign, getter = isSelectorReady) BOOL selectorReady;

@property (nonatomic, readwrite , weak  ) LAnnotationType           selectionType;

@property (nonatomic, readonly	, strong) LGLCanvas                 *colorMap;

@property (nonatomic, readonly	, strong) NSMutableArray            *annotations;
@property (nonatomic, readonly  , strong) NSMutableIndexSet         *discarded;

@end


@implementation LAnnotator
{
    union {
        GLint index;

        struct {
            GLubyte
            red     ,
            green   ,
            blue    ,
            alpha   ;
        };
    } __colorMapping;
}

#pragma mark - Static Objects
//*********************************************************************************************************************//

static const LAnnotationType kLAnnotationTypeAll = (^(__UNUSEDARG(NSArray *)) { return YES; });


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

+ (instancetype)annotatorWithDelegate:(id <LADelegate>)delegate
{
    return [[self alloc] initWithDelegate:delegate];
}

- (instancetype)init
{
    if ((self = [self initWithDelegate:nil])) {

    }

    return self;
}

- (instancetype)initWithDelegate:(id <LADelegate>)delegate
{
	if ((self = [super init])) {
		_delegate       = delegate;

		_annotations	= [NSMutableArray new];
        _discarded      = [NSMutableIndexSet new];

        _selectionType  = kLAnnotationTypeAll;
	}

	return self;
}

- (void)dealloc
{
    /**
     * Annotations must be deallocated before the annotator.
     */
    [_annotations removeAllObjects];

	DBGMessage(@"deallocated %@", self);
}


#pragma mark - Properties
//*********************************************************************************************************************//

@dynamic colorMap;

- (LGLCanvas *)colorMap
{
	NSString    *key        = (@"target-canvas");
	LGLCanvas   *colorMap   = [LGL objectPool][key];

	if (!colorMap) {
		colorMap = [LGL objectPool][key]
        =
        [[LGLCanvas alloc]
         initWithWidth  :kLGLSurfaceCanvasDimension
         height         :kLGLSurfaceCanvasDimension];
	}

	return colorMap;
}

@dynamic annotationCount;

- (NSUInteger)annotationCount
{
    return [_annotations count];
}

@dynamic lastAnnotation;

- (LAnnotation *)lastAnnotation
{
	return [_annotations lastObject];
}

@synthesize undoManager = _undoManager;

- (NSUndoManager *)undoManager
{
    if (!_undoManager) {
        _undoManager = [NSUndoManager new];
    }

    return _undoManager;
}

@synthesize annotationTool = _annotationTool;

- (void)setAnnotationTool:(LATool *)annotationTool
{
    [self.annotationTool destroyObserversForKeyPath:@Keypath(self.annotationTool.buffer)];

    {_annotationTool = annotationTool;}

    [self.annotationTool destroyObserversForKeyPath:@Keypath(self.annotationTool.buffer)];

    if (annotationTool) {
        $weakify(self) {
            [annotationTool observerForKeyPath:@Keypath(annotationTool.buffer)] (^{
                $strongify(self) {
                    [self annotateWithTool:self.annotationTool];
                }
            });
        }
    }
}

@synthesize hidden = _hidden;

- (void)setHidden:(BOOL)hidden
{
	if (_hidden != hidden) {
		{_hidden = hidden;}

        for (LAnnotation *annotation in _annotations) {
            if (![annotation isEmpty]) {
                [_delegate annotatorDidUpdate:self];
                break;
            }
        }
	}
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (LAnnotation *)annotationAtIndex:(NSUInteger)index
{
    if ([_annotations count] > index) {
        return _annotations[index];
    }

	return nil;
}

- (NSUInteger)annotationIndexForPoint:(CGPoint)point
{
    return [self indexOfAnnotationWithType:kLAnnotationTypeAll atPoint:point];
}

- (void)createAnnotationWithTool:(LATool *)tool
{
    DBGParameterAssert(tool != nil);

    $weakify(self) {
        [tool initializedObserverForKeyPath:@Keypath(tool.buffer)] (^{
            $strongify(self) {
                [self annotateWithTool:tool];
            }
        });
    }
}

- (void)addAnnotation:(LAnnotation *)annotation
{
    DBGParameterAssert(annotation != nil);

    [self addAnnotations:@[ annotation ]];
}

- (void)insertAnnotation:(LAnnotation *)annotation atIndex:(NSUInteger)index
{
    DBGParameterAssert(annotation != nil);

    [self
     insertAnnotations  :@[ annotation ]
     atIndexes          :
     [NSIndexSet indexSetWithIndexesInRange:
      ((NSRange) {
         index,
         1
     })]];
}

- (void)addAnnotations:(NSArray *)annotations
{
    [self
     insertAnnotations  :annotations
     atIndexes          :
     [NSIndexSet indexSetWithIndexesInRange:
      ((NSRange) {
         [_annotations count],
         [annotations count]
     })]];
}

- (void)insertAnnotations:(NSArray *)annotations atIndexes:(NSIndexSet *)indexes
{
    DBGParameterAssert([annotations count] == [indexes count]);

    NSUInteger  currentIndex    = [indexes firstIndex];
    BOOL        needsRender     = NO;

    for (LAnnotation *annotation in annotations) {
        if (currentIndex > [_annotations count]) {
            [_annotations addObject:annotation];
        } else {
            [_annotations insertObject:annotation atIndex:currentIndex];
        }

        if (![annotation isEmpty]) {
            needsRender = YES;
        }

        currentIndex = [indexes indexGreaterThanIndex:currentIndex];
    }

    if (needsRender) {
        [_delegate annotatorDidUpdate:self];
    }
}

- (void)removeAllAnnotations
{
    [self removeAnnotationsAtIndexes:
     [NSIndexSet indexSetWithIndexesInRange:
      ((NSRange) {
         (0), [_annotations count]
     })]];
}

- (void)removeLastAnnotation
{
    if ([_annotations count]) {
        [self removeAnnotationAtIndex:[_annotations count]-1];
    }
}

- (void)removeAnnotation:(LAnnotation *)annotation
{
    if (annotation) {
        NSInteger index = [_annotations indexOfObject:annotation];
        [self removeAnnotationAtIndex:index];
    }
}

- (void)removeAnnotationAtIndex:(NSUInteger)index
{
    if (index != NSNotFound) {
        [self removeAnnotationsAtIndexes:[NSIndexSet indexSetWithIndex:index]];
    }
}

- (void)removeAnnotationsAtIndexes:(NSIndexSet *)indexes
{
    NSUInteger count = [_annotations count];

    if ((count > [indexes firstIndex]) &&
        (count > [indexes lastIndex])) {
        [_annotations removeObjectsAtIndexes:indexes];
        [_delegate annotatorDidUpdate:self];
    }
}

- (void)render
{
	if (!_hidden) {
        [LGL setBlendingEnabled:GL_TRUE];

        NSUInteger index = 0;

        for (LAnnotation *annotation in _annotations) {
            [annotation render];
            [annotation.actions makeObjectsPerformSelector:@selector(invoke)];

            {_rendererDirty = YES;}

            ++ index;
		}
	}
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)annotateWithTool:(LATool *)tool
{
    DBGParameterAssert(tool != nil);

    /**
     * Begin the annotation. If it exists, a
     * previous annotation will be compressed.
     */
    if (![tool.buffer length]) {
        LAnnotation *annotation
        =
        [self lastAnnotation];
        [annotation packVertices];

        if (![tool isEditor]) {
            {_selectionMode = NO;}

            if (![annotation isEmpty]) {
                UIColor     *color      = [tool.properties colorWithPremultipliedAlpha];
                LAnnotation *annotation = [LAnnotation new];

                [annotation setColor:color];
                [self addAnnotation:annotation];
            }
        } else {
            if ((_selectionMode) || (_rendererDirty)) {
                {_selectionMode = YES;}
                {_selectorReady = NO;}
            }
        }
    }
    /**
     * Continue the annotation.
     */
    else {
        if (![tool isEditor]) {
            if ([tool isShaper]) {
                [self.lastAnnotation setVertexData:tool.buffer];
            } else {
                [self.lastAnnotation appendVertexData:tool.buffer];
            }

            [_delegate annotatorDidUpdate:self];
        }
    }
}

- (void)prepareForPickingAnnotationOfType:(LAnnotationType)type
{
    [_discarded removeAllIndexes];

    typeof(__colorMapping) *colorMapping = &__colorMapping;

    [self.colorMap clear];
    [self.colorMap queueRenderOperation:^{
        [LGL setBlendingEnabled:GL_FALSE];

        colorMapping->index = 0;

        for (LAnnotation *annotation in _annotations) {
            if (type(annotation.actions)) {
                UIColor *store = [annotation color];
                {
                    [annotation setColor:
                     [UIColor
                      colorWithRed :(colorMapping->red    /255.)
                      green        :(colorMapping->green  /255.)
                      blue         :(colorMapping->blue   /255.)
                      alpha        :(colorMapping->alpha  /255.)]];
                    [annotation render];
                }
                [annotation setColor:store];
            }

            ++ colorMapping->index;
        }

        /**
         * glFinish() is needed, otherwise offscreen rendering does not 
         * work. glFlush() cannot be used here, since color picking reads 
         * pixels in the CVPixelBuffer, which is CPU-bound. It is possible 
         * for the CVPixelBuffer to be out of date if GPU instructions have 
         * not finished executing yet.
         */
        glFinish();
    }];
}


#pragma mark - Protected Methods
//*********************************************************************************************************************//

- (NSArray *)actionsForAnnotation:(LAnnotation *)annotation
{
    return [annotation actions];
}

- (void)addAction:(NSInvocation *)action toAnnotation:(LAnnotation *)annotation
{
    [annotation.actions addObject:action];
}

- (void)removeActionAtIndex:(NSUInteger)index fromAnnotation:(LAnnotation *)annotation
{
    [annotation.actions removeObjectAtIndex:index];
}

- (NSUInteger)indexOfAnnotationWithType:(LAnnotationType)type atPoint:(CGPoint)point
{
    DBGParameterAssert(isgreaterequal(point.x, 0.) &&
                        isgreaterequal(point.y, 0.));

    if (((_selectionMode == YES) && (_selectorReady != YES)) ||
        ((_selectionMode != YES) && (_rendererDirty == YES)) ||
        ![_selectionType isEqual:type])
    {
        {_selectionType = type;}
        {_selectorReady = YES;}

        [self prepareForPickingAnnotationOfType:type];
    }

	LGLVector4 rgba;
    [[self.colorMap colorAtPoint:point] getComponents:rgba.v];

    {_rendererDirty = NO;}

    __colorMapping.red      = (rgba.r *255.);
	__colorMapping.green    = (rgba.g *255.);
	__colorMapping.blue     = (rgba.b *255.);
	__colorMapping.alpha    = (rgba.a *255.);

    NSUInteger index = ((__colorMapping.index <
                         [_discarded count] +
                         [_annotations count]) &&
                        (__colorMapping.index >= 0)?
                        (__colorMapping.index):
                        (NSNotFound));

    if (index != NSNotFound) {
        if (![_discarded containsIndex:index]) {
            [_discarded addIndex:index];

            NSUInteger adjusted = index;
            while (
                  (index = [_discarded indexLessThanIndex:index])
                  !=
                  (NSNotFound)
                  &&
                  (--adjusted)
            );

            return adjusted;
        }
    }

	return NSNotFound;
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LAnnotation
#pragma mark -
//*********************************************************************************************************************//




@interface LAnnotation ()

@property (nonatomic, readwrite, assign) LGLIMatrix2x2  viewport;

@property (nonatomic, readwrite, assign) GLfloat        scale;

@end

@implementation LAnnotation

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithTransform:(LGLMatrix4x4)transform __ILLEGALMETHOD__;

- (instancetype)init
{
    if ((self = [super initWithTransform:LGLMatrix4x4Identity])) {
        _actions = [NSMutableArray new];
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)render
{
    if (!IsEqual(_scale, LGL.scale) ||
        !LGLIMatrix2x2AllEqualToIMatrix2x2
        ([self viewport], [LGL viewport]))
    {
        _viewport = [LGL viewport];
        _scale    = [LGL scale];

        GLuint surfaceWidth  = LFMDivf(_viewport.w, _scale);
        GLuint surfaceHeight = LFMDivf(_viewport.h, _scale);

        [self setTransform:
         LGLMatrix4x4MakeOrtho
         (+0.           , +surfaceWidth ,
          +surfaceHeight, +0.           ,
          -2.           , +2.           )];
    }

    [super render];
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (BOOL)isEmpty
{
    return (![self length] && ![_actions count]);
}

@end