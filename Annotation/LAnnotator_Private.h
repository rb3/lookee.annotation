//
//  LAnnotator_Private.h
//  Writeability
//
//  Created by Ryan on 3/1/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LAnnotator.h"


@interface LAnnotator ()

- (NSUInteger)indexOfAnnotationWithType:(LAnnotationType)type atPoint:(CGPoint)point;

- (NSArray *)actionsForAnnotation:(LAnnotation *)annotation;

- (void)addAction:(NSInvocation *)action toAnnotation:(LAnnotation *)annotation;
- (void)removeActionAtIndex:(NSUInteger)index fromAnnotation:(LAnnotation *)annotation;

@end
