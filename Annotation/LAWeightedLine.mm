//
//  LAWeightedLine.h
//  Writeability
//
//  Created by Ryan on 9/17/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LAWeightedLine.h"

#import <Utilities/LFastMath.h>



//*********************************************************************************************************************//
#pragma mark -
#pragma mark LAWeightedLine
#pragma mark -
// Based on SiegeLord's arc drawing algorithm @ https://www.allegro.cc/forums/thread/594175
//*********************************************************************************************************************//



#pragma mark - Function Declarations
//*********************************************************************************************************************//

static void LAWeightedLineCap
(
 LGLVector2         from,
 LGLVector2         to,
 LGLVector2         center,
 float              angleDelta,
 float              radius,
 std::vector<float> &cap
 );


#pragma mark - Public Functions
//*********************************************************************************************************************//

void LAWeightedLineSegment
(
 LGLVector2         head,
 LGLVector2         tail,
 float              headWeight,
 float              tailWeight,
 std::vector<float> *body,
 std::vector<float> *headCap,
 std::vector<float> *tailCap
 ) {
	DBGAssert(body);

	float   headRadius  = LFMDivf(headWeight, 2.);
	float   tailRadius  = LFMDivf(tailWeight, 2.);
	
	float   distance    = LGLVector2Distance(head, tail);

	float   xDelta      = (tail.x - head.x);
	float   yDelta      = (tail.y - head.y);
	float   angle       = (LFMAtan2f(xDelta, yDelta) - M_PI);

	LGLVector2 headStart;
	LGLVector2 headEnd;

	if (headCap) {
		float tangentAngle  = LFMAtan2f((headWeight - tailWeight), (distance *2.));

		float startAngle    = (-tangentAngle - angle);
		float angleDelta    = (+tangentAngle * 2. + M_PI);

		float hOffset       = (headRadius * LFMCosf(startAngle));
		float vOffset       = (headRadius * LFMSinf(startAngle));

		headStart	= ((LGLVector2) {
			.x = (head.x + hOffset),
			.y = (head.y + vOffset)
		});

		headEnd		= ((LGLVector2) {
			.x = (head.x - hOffset),
			.y = (head.y - vOffset)
		});

        headCap->push_back(headStart.x);
        headCap->push_back(headStart.y);

		LAWeightedLineCap(headStart, headEnd, head, angleDelta, headRadius, *headCap);
	} else {
        float hOffset;
        float vOffset;

		if (IsZero(distance)) {
            hOffset = 0.;
            vOffset = 0.;
        } else {
            hOffset = LFMDivf((headRadius * yDelta), distance);
            vOffset = LFMDivf((headRadius * -xDelta), distance);
        }

		headEnd		= ((LGLVector2) {
			.x = (head.x + hOffset),
			.y = (head.y + vOffset)
		});

		headStart	= ((LGLVector2) {
			.x = (head.x - hOffset),
			.y = (head.y - vOffset)
		});

        body->push_back(headEnd.x);
        body->push_back(headEnd.y);

        body->push_back(headEnd.x);
        body->push_back(headEnd.y);
	}

    body->push_back(headStart.x);
    body->push_back(headStart.y);

	LGLVector2 tailStart;
	LGLVector2 tailEnd;

	if (tailCap) {
		float tangentAngle  = LFMAtan2f((tailWeight - headWeight), (distance *2.));

		float startAngle    = (+tangentAngle - angle);
		float angleDelta    = (-tangentAngle * 2. - M_PI);

		float hOffset       = (tailRadius * LFMCosf(startAngle));
		float vOffset       = (tailRadius * LFMSinf(startAngle));

		tailStart	= ((LGLVector2) {
			.x = (tail.x + hOffset),
			.y = (tail.y + vOffset)
		});

		tailEnd		= ((LGLVector2) {
			.x = (tail.x - hOffset),
			.y = (tail.y - vOffset)
		});

        tailCap->push_back(tailEnd.x);
		tailCap->push_back(tailEnd.y);

		LAWeightedLineCap(tailStart, tailEnd, tail, angleDelta, tailRadius, *tailCap);
	} else {
        float hOffset;
        float vOffset;

        if (IsZero(distance)) {
            hOffset = 0.;
            vOffset = 0.;
        } else {
            hOffset = LFMDivf((tailRadius * yDelta), distance);
            vOffset = LFMDivf((tailRadius * -xDelta), distance);
        }

		tailStart	= ((LGLVector2) {
			.x = (tail.x + hOffset),
			.y = (tail.y + vOffset)
		});

		tailEnd		= ((LGLVector2) {
			.x = (tail.x - hOffset),
			.y = (tail.y - vOffset)
		});

        body->push_back(tailStart.x);
		body->push_back(tailStart.y);
	}

    body->push_back(tailEnd.x);
	body->push_back(tailEnd.y);

	body->push_back(tailEnd.x);
	body->push_back(tailEnd.y);
}


#pragma mark - Protected Functions
//*********************************************************************************************************************//

static void LAWeightedLineCap
(
 LGLVector2         from,
 LGLVector2         to,
 LGLVector2         center,
 float              angleDelta,
 float              radius,
 std::vector<float> &cap
 ) {
    unsigned int    vertexCount     = (LFMCeilf(LFMLog2f(fmaxf(radius, 8.))) *2);
	unsigned int    segmentCount    = (LFMFloorf(LFMDivf((LFMFabsf(angleDelta) * vertexCount), M_PI)) +1);

	float angleStep                 = LFMDivf(angleDelta, segmentCount);
	float tangetialFactor           = (0.+ LFMTanf(angleStep));
	float radialFactor              = (1.- LFMCosf(angleStep));

	LGLVector2	vertex	= from;
	LGLVector2	bottom;

	for (unsigned int counter = 0;counter <= segmentCount; ++ counter) {
        cap.push_back(vertex.x);
        cap.push_back(vertex.y);

		bottom	= LGLVector2Lerp(from, to, LFMDivf(counter, segmentCount));

        cap.push_back(bottom.x);
        cap.push_back(bottom.y);

		float xTangent = -(vertex.y - center.y);
		float yTangent = +(vertex.x - center.x);

		vertex.x += (xTangent * tangetialFactor);
		vertex.y += (yTangent * tangetialFactor);

		float xRadial = +(center.x - vertex.x);
		float yRadial = +(center.y - vertex.y);

		vertex.x += (xRadial * radialFactor);
		vertex.y += (yRadial * radialFactor);
	}
}