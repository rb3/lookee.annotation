//
//  LAPenTool.m
//  Writeability
//
//  Created by Ryan on 9/15/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LAPenTool.h"

#import "LABezier.h"
#import "LAWeightedLine.h"

#import <Rendering/LGLUtilities.h>

#import <vector>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LAPenTool
#pragma mark -
//*********************************************************************************************************************//




@implementation LAPenTool

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
	GLubyte _stage;

	GLfloat _weight;
	GLfloat _store;

    std::vector<GLfloat>    _vertices;
    std::vector<LGLVector2> _cache;
    std::vector<LGLVector2> _bezier;
}


#pragma mark - LAProtocol
//*********************************************************************************************************************//

- (void)initializeTool
{
}


- (GLvoid)beginAtVector:(LGLVector2)vector
{
	{_stage = 0;}

    _weight = [self.properties scaledWeight];
	_store  = [self.properties scaledWeight];

    _cache.clear();
	_vertices.clear();

    _cache.push_back(vector);
    _cache.push_back(vector);
    _cache.push_back(vector);
}

- (GLvoid)moveToVector:(LGLVector2)vector
{
    _cache.push_back(vector);

	if (_cache.size() >= 4) {
#define head1       _cache[0]
#define control1    _cache[1]
#define tail1       _cache[2]
#define control2    _cache[3]

		if (_stage != 2) {
			tail1 = LGLVector2Lerp(control1, control2, .5);
		}

		if (_stage != 0) {
            _bezier.push_back(head1);
		}

		LABezierQuadric(head1, control1, tail1, _bezier);
        _bezier.push_back(tail1);

        _vertices.clear();

		GLfloat distance	= (LGLVector2Distance(head1, control1) + LGLVector2Distance(control1, tail1));
		GLfloat ratio		= (!IsZero(distance)? fminf(LFMRecf(distance *.04), 1.): 1.);

		GLfloat weight		= (_weight * ratio);

        GLuint  index       = 0;
        GLuint  length      = (_bezier.size() -1);

        if (_stage == 0) {
            {_stage = 1;}

            LAWeightedLineSegment(_bezier[index +0],
								 _bezier[index +1],
								 _store,
								 LFMCosineInterpolate(_store, weight, LFMRecf(length)),
								 &_vertices,
								 &_vertices,
								 &_vertices);

            ++ index;
        }

		for (; index < length; ++ index) {
			GLfloat headWeight = LFMCosineInterpolate(_store, weight, LFMDivf((index +0.), length));
			GLfloat tailWeight = LFMCosineInterpolate(_store, weight, LFMDivf((index +1.), length));
			
			LAWeightedLineSegment(_bezier[index +0],
								 _bezier[index +1],
								 headWeight,
								 tailWeight,
								 &_vertices,
								 NULL,
								 &_vertices);
		}

		[self setVertexData:WrapSTLVector(_vertices)];

        _bezier.clear();
        _cache.erase(_cache.begin(), _cache.begin() +2);

		{_store = weight;}

#undef head1
#undef control1
#undef tail1
#undef control2
	}
}

- (GLvoid)endAtVector:(LGLVector2)vector
{
	if (_stage == 1) {
		{_stage = 2;}

        _cache.push_back(vector);
	}

	[self moveToVector:vector];
}

@end
